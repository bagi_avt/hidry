**Добро пожаловать**

*Здесь представлен список страниц домашней работы:*

---
*    [home page](https://bagi-avt.github.io/hidry/) - главная страница;
*    [mobile menu](https://bagi-avt.github.io/hidry/pages/mobile-menu/) - меню заголовка (смотреть в мобильной версии сайта);
*    [dog list](https://bagi-avt.github.io/hidry/pages/dogs/) - список собак;
*    [menu with filter](https://bagi-avt.github.io/hidry/pages/left-menu/) - страница демонстрирующая меню с фильтром (смотреть меняя ширину экрана);
*    [basket](https://bagi-avt.github.io/hidry/pages/basket/) - страница карзины;
*    [more](https://bagi-avt.github.io/hidry/pages/more/) - страница с подробной информацией;
*    [checkout](https://bagi-avt.github.io/hidry/pages/checkout/) - страница оформления заказа.

---
